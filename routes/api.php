<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\v1'], function () {
	Route::post('login', 'LoginController@login');
	Route::post('register', 'LoginController@register');
	Route::group(['middleware' => 'jwt.apiauth'], function () {
		Route::post('logout', 'LoginController@logout');
		// Route::get('address','AddressController@index');
		Route::get('address/ajax','AddressController@ajax');
		Route::post('address', 'AddressController@store');
		Route::get('address/{address}', 'AddressController@show');
		Route::put('address/{address}', 'AddressController@update');
		Route::delete('address/{address}', 'AddressController@destroy');

		Route::get('profile','UserController@viewProfile')->name('m.profile.view');
		Route::post('profile','UserController@updateProfile')->name('m.profile.update');
	});	
});
