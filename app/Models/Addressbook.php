<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Addressbook extends Model
{
    public $timestamps = true;

    protected $table = "address_book";
    protected $fillable = [
    	'title', 'name', 'number', 'address_line_1', 'address_line_2', 'address_line_3', 'pincode',
    	'city_slug', 'state_slug', 'country_slug', 'status', 'is_default', 'type'
    ];
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public static function addRules(){
    	return [
    		'title'=>'required|max:30|min:2',
            'name'=>'required|max:30|min:2',
            'number'=>'required|digits:10',
            'address_line_1'=>'required|min:5|max:50',
            'address_line_2'=>'required|min:2|max:50',
            'address_line_3'=>'max:50',
            'pincode'=>'required|digits:6',
            'city_slug'=>'required',
            'state_slug'=>'required',
            'country_slug'=>'required',
            'is_default'=>'in:1,0',
            'type'=>'required|in:from,to',
    	];
    }

    public static function editRules(){
    	return [
    		'title'=>'required|max:30|min:2',
            'name'=>'required|max:30|min:2',
            'number'=>'required|digits:10',
            'address_line_1'=>'required|min:5|max:50',
            'address_line_2'=>'required|min:2|max:50',
            'address_line_3'=>'max:50',
            'pincode'=>'required|digits:6',
            'city_slug'=>'required',
            'state_slug'=>'required',
            'country_slug'=>'required',
            'is_default'=>'in:1,0',
            'type'=>'required|in:from,to',
    	];
    }

    public static function removeDefaults($user_id,$type){
        return static::where([
            ['is_default','=',1],
            ['type','=',$type]
        ])->update(['is_default' => 0]);
    }
}
