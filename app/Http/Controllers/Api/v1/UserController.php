<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class UserController extends Controller
{
    public function viewProfile(){
    	$user = auth()->guard('api')->user();
    	$data = [
    		'name' => $user->name,
    		'email' => $user->email,
    	];
    	return response()->json(['status' => 'Success', 'data' => $data], 200);
    }

    public function updateProfile(Request $request){
    	$validator = Validator::make($request->all(), [
            'name'=>'required|max:30|min:2'
        ]);

        if($validator->fails()){
            return response(['status' => 'error','errors' => $validator->errors()], 400);
        }

    	$user = auth()->guard('api')->user();
    	$user->name = $request->name;
    	$user->save();
    	return response(['status' => 'Success','messages' => 'Profile Updated Successfully!'], 200);
    }

}
