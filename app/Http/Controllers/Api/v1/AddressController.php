<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Addressbook;
use Yajra\Datatables\Datatables;
use Validator;

class AddressController extends Controller{

    public function ajax(){
        $user = auth()->guard('api')->user();
        $address = Datatables::of($user->addresses);
        $address->addColumn('address', function ($address) {
            return  $address->address_line_1 . "<br>" .
                    $address->address_line_2 . "<br>" .
                    $address->address_line_3 . "<br>" . 
                    $address->country_slug ." | ". $address->state_slug ." | ". 
                    $address->city_slug ." (".$address->pincode.")";
        });
        $address->addColumn('default', function ($address) {
            return  $address->is_default == 1 ? 'Default' : 'Normal';
        });
        $address->addColumn('actions', function ($address) {
            $cta = '<button class="btn btn-xs btn-info" onclick="addressEdit('.$address->id.')" title="view">Edit</button>';

            $cta .= '<button class="btn btn-xs btn-danger" onclick="addressDelete('.$address->id.')" type="button">Delete</button>';
            return $cta;
        })->rawColumns(['actions','address']);
        return $address->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Addressbook::addRules());

        if($validator->fails()){
            return response(['status' => 'error','errors' => $validator->errors()], 400);
        }
        $user = auth()->guard('api')->user();

        if(isset($request->is_default)){
            Addressbook::removeDefaults($user->id,$request->type);
        }

        $address = new Addressbook($request->all());
        $address->user_id = $user->id;
        $address->save();

        return response(['status' => 'Success','messages' => 'Address Added Successfully!'], 200);
    }
	
    public function show(Addressbook $address){
    	return response()->json(['status' => 'Success', 'data' => $address], 200);
    }

	public function update(Request $request, Addressbook $address)
    {
        $validator = Validator::make($request->all(), Addressbook::editRules());
        if($validator->fails()){
            return response(['status' => 'error','errors' => $validator->errors()], 400);
        }
        $user = auth()->guard('api')->user();
        if(isset($request->is_default)){
            Addressbook::removeDefaults($user->id,$request->type);
        }else{
            $request->request->add(['is_default' => 0]);
        }
        $address->update($request->all());

        return response(['status' => 'Success','messages' => 'Address Updated Successfully!'], 200);
    }

    public function destroy(Addressbook $address)
    {
        $address->delete();
        return response(['status' => 'Success','messages' => 'Address Deleted Successfully!'], 200);
    }
}