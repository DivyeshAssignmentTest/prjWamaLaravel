<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Addressbook;
use Yajra\Datatables\Datatables;
use Validator;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('addressbook.list');
    }

    public function ajax(){
        $user = auth()->guard('web')->user();
        $address = Datatables::of($user->addresses);
        $address->addColumn('address', function ($address) {
            return  $address->address_line_1 . "<br>" .
                    $address->address_line_2 . "<br>" .
                    $address->address_line_3 . "<br>" . 
                    $address->country_slug ." | ". $address->state_slug ." | ". 
                    $address->city_slug ." (".$address->pincode.")";
        });
        $address->addColumn('default', function ($address) {
            return  $address->is_default == 1 ? 'Default' : 'Normal';
        });
        $address->addColumn('actions', function ($address) {
            $cta = '<a class="btn btn-xs btn-info" href="'.route('address.edit',$address).'" title="view">Edit</a>';

            $cta .= '<form onSubmit="return confirmDelete()" style="display:inline;" method="POST" action="'.route('address.destroy', ['id' => $address->id]).'">'.csrf_field().'
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-xs btn-danger" type="submit" title="Delete">Delete</button>
                    </form>';
            return $cta;
        })->rawColumns(['actions','address']);
        return $address->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $address = new Addressbook();
        $types = ['from' => 'From', 'to' => 'To'];
        $default = [1 => 'Default', 0 => 'Normal'];
        $country = ['india' => 'India'];
        $state = ['maharashtra' => 'Maharashtra'];
        $city = ['mumbai' => 'Mumbai', 'thane' => 'Thane',];
        return view('addressbook.add',compact('address','types','default','country','state','city'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Addressbook::addRules());

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $user = auth()->guard('web')->user();

        if(isset($request->is_default)){
            Addressbook::removeDefaults($user->id,$request->type);
        }

        $address = new Addressbook($request->all());
        $address->user_id = $user->id;
        $address->save();

        return redirect()->route('address.index')->with('success', 'Address Added Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Addressbook $address)
    {
        $types = ['from' => 'From', 'to' => 'To'];
        $default = [1 => 'Default', 0 => 'Normal'];
        $country = ['india' => 'India'];
        $state = ['maharashtra' => 'Maharashtra'];
        $city = ['mumbai' => 'Mumbai', 'thane' => 'Thane',];
        return view('addressbook.edit',compact('address','types','default','country','state','city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Addressbook $address)
    {
        $validator = Validator::make($request->all(), Addressbook::editRules());
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $user = auth()->guard('web')->user();
        if(isset($request->is_default)){
            Addressbook::removeDefaults($user->id,$request->type);
        }else{
            $request->request->add(['is_default' => 0]);
        }
        $address->update($request->all());

        return redirect()->route('address.index')->with('success','Address Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Addressbook $address)
    {
        $address->delete();
        return redirect()->route('address.index')->with('success','Address Deleted successfully!');
    }
}
