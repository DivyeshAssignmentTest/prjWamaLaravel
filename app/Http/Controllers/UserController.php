<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    public function viewProfile()
    {
    	$user = auth()->guard('web')->user();
        return view('profile',compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|max:30|min:2'
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
    	$user = auth()->guard('web')->user();
    	$user->name = $request->name;
    	$user->save();
        return redirect()->route('profile.view')->with('success', 'Profile Updated Successfully!');
    }
}
