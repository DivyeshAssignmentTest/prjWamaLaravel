<?php

namespace App\Http\Middleware;

use Closure;
Use Auth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class ApiAuth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request instanceof \Dingo\Api\Http\InternalRequest) {
            
            if(Auth::user()) {
                Auth::guard('api')->onceUsingId(Auth::user()->id);
                return $next($request);
            } else {
                return response(['status' => 'error', 'message' => 'Internal Call Unauthenticated'], 400);
            }
        } else {
            
            if (! $token = $this->auth->setRequest($request)->getToken()) {
                return response(['status' => 'error', 'message' => 'token_not_provided'], 400);
            }

            try {
                $user = $this->auth->authenticate($token);
            } catch (TokenExpiredException $e) {
                return response(['status' => 'error','message' => 'token_expired'], $e->getStatusCode());
            } catch (JWTException $e) {
                return response(['status' => 'error','message' => 'token_invalid'], $e->getStatusCode());
            } catch (\Exception $e) {
                return response(['status' => 'error','message' => 'token_invalid'], 401);
            }

            if (! $user) {
                return response(['status' => 'error','message' => 'user_not_found'], 404);
            }

            return $next($request);
        }
    }
}
