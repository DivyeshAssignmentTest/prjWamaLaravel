var $image = $('#image');
var $dataRotate = $('#dataRotate');
var options = {
    aspectRatio: 1 / 1,
    preview: '.img-preview',
    viewMode: 2, //There 3 viewModes = viewMode 1,viewMode 2,viewMode 3
    minContainerWidth:650,
    minContainerHeight:300

};
$image.on({
//    ready: function (e) {
//        console.log(e.type);
//    },
//    cropstart: function (e) {
//        console.log(e.type, e.detail.action);
//    },
//    cropmove: function (e) {
//        console.log(e.type, e.detail.action);
//    },
//    cropend: function (e) {
//        console.log(e.type, e.detail.action);
//    },
//    crop: function (e) {
//        console.log(e.type);
//    },
//    zoom: function (e) {
//        console.log(e.type, e.detail.ratio);
//    } 
}).cropper(options);



function getImageData() {
    var cropper = $image.data('cropper');
    var cropcanvas = cropper.getCroppedCanvas({width:128,hight:128}); //image Quality redusing to 250 by 250
    var croppng = cropcanvas.toDataURL("image/jpg");
    // console.log("cropcanvas",cropcanvas);
    console.log("croppng", croppng);
    var str = '<img src="' + croppng + '">'
    $cropedImage = croppng;
    $('#imgdiv').html(str);
}

function getCroppedImage(){
    return $cropedImage;
}
// Import image
var uploadedImageName = 'cropped.jpg';
var uploadedImageType = 'image/jpeg';
var uploadedImageURL;
var $inputImage = $('#inputImage');
var $cropedImage = null;

if (URL) {
    $inputImage.change(function () {
        var files = this.files;
        var file;

        if (!$image.data('cropper')) {
            return;
        }

        if (files && files.length) {
            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                uploadedImageName = file.name;
                uploadedImageType = file.type;

                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                }

                uploadedImageURL = URL.createObjectURL(file);
                $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                $inputImage.val('');
            } else {
                window.alert('Please choose an image file.');
            }
        }
    });
} else {
    $inputImage.prop('disabled', true).parent().addClass('disabled');
}

function rotateRight(){
    $image.cropper('rotate', -45);
}
function rotateLeft(){
    $image.cropper('rotate', 45);
}
function filpHorizontal(){
    var data = $image.cropper('getData');
    $image.cropper('scale', -data.scaleX, data.scaleY);
}
function filpVertical(){
    var data = $image.cropper('getData');
    $image.cropper('scale', data.scaleX, -data.scaleY);
}
    