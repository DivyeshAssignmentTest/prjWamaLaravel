<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_book', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->references('id')->on('users');;
            $table->string('title');
            $table->string('name');
            $table->bigInteger('number');
            $table->string('address_line_1');
            $table->string('address_line_2');
            $table->string('address_line_3')->nullable(true);
            $table->integer('pincode');
            $table->string('city_slug');
            $table->string('state_slug');
            $table->string('country_slug');
            $table->tinyInteger('is_default')->default(0);
            $table->enum('type',['from','to']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_book');
    }
}
