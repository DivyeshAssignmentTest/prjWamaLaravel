function sendAjax(options, isThirdParty) {
    return $.ajax(options);
}

$("#btn_register").on("click", function(){
    removeErrors($('#frm_register'));
    var response = sendAjax({
        url : apiUrl + "register",
        method : 'post',
        async : false,
        data : $('#frm_register').serialize()
    },true);

    response.done(function(dataStr) {
        login();
    }).fail(function(jqXHR, textStatus, error) {
        var data = jqXHR.responseJSON;
        setErrors(data.errors);
    });
});

function login(){
    var response = sendAjax({
        url : apiUrl + "login",
        method : 'post',
        async : false,
        data : $('#frm_register').serialize()
    },true);

    response.done(function(dataStr) {
        sessionStorage.setItem("token", dataStr.access_token);
        window.location.href = "dashboard.html";
    }).fail(function(jqXHR, textStatus, error) {
        alert("invalid credentials");
    });
}

function removeErrors(form) {
    form.find('span').html('');
}

function setErrors(errors) {
    for (var key in errors){
        var input = $(`[name=${key}]`).closest('.form-group');
        var elm = input.find('span');
        var ul = '<ul class="location-style">';
        errors[key].forEach(function(val) {
            ul += `<li style="color:red;">${val}</li>`;
        });
        ul += '</ul>';
        elm.html(ul);
        input.addClass("has-error has-danger");
    }
}