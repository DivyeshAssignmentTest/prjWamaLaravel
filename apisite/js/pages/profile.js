$(document).ready(function(){
	$(`[name=name]`).val(sessionStorage.getItem("username"));
	$(`[name=email]`).val(sessionStorage.getItem("email"));
});


$("#btn_save").click(function(){
	var response = sendAjax({
	    url : apiUrl + "profile",
	    method : 'post',
	    headers : {
	    	'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
	    },
	    async : false,
	    data : $('#frm_address').serialize()
	},true);

	response.done(function(dataStr) {
	    window.location.href = "dashboard.html";
	}).fail(function(jqXHR, textStatus, error) {
	    console.log(jqXHR,textStatus,error);
	    var data = jqXHR.responseJSON;
	    setErrors(data.errors);
	});
});
