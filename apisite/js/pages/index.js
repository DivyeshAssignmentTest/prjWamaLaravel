function sendAjax(options, isThirdParty) {
    return $.ajax(options);
}

$("#btn_login").on("click", function(){
    var response = sendAjax({
        url : apiUrl + "login",
        method : 'post',
        async : false,
        data : $('#loginform').serialize()
    },true);

    response.done(function(dataStr) {
        sessionStorage.setItem("token", dataStr.access_token);
        window.location.href = "dashboard.html";
    }).fail(function(jqXHR, textStatus, error) {
        console.log(jqXHR,textStatus,error);
        alert("invalid credentials");
    });
});

function removeErrors(form) {
    form.find('span').html('');
}

function setErrors(errors) {
    for (var key in errors){
        var input = $(`[name=${key}]`).closest('.form-group');
        var elm = input.find('span');
        var ul = '<ul class="location-style">';
        errors[key].forEach(function(val) {
            ul += `<li style="color:red;">${val}</li>`;
        });
        ul += '</ul>';
        elm.html(ul);
        input.addClass("has-error has-danger");
    }
}

function clearSession(){
    console.log('called');
    sessionStorage.clear();
}
clearSession();