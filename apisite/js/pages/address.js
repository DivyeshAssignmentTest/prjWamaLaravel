var ajaxUrl = apiUrl+"address/ajax";

function addressAdd(){
    window.location.href = "add_address.html"
}

function addressEdit(id){
    sessionStorage.setItem("address_id", id);
    window.location.href = "edit_address.html"
}

function addressDelete(id){
    if(confirmDelete()){
        var response = sendAjax({
            url : apiUrl + "address/" + id,
            method : 'post',
            headers : {
                'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
            },
            async : false,
            data : '_method=Delete'
        },true);

        response.done(function(dataStr) {
            window.location.reload();
        }).fail(function(jqXHR, textStatus, error) {
            var data = jqXHR.responseJSON;
            console.log(data.errors);
        });
    }
}

function confirmDelete(){
    return confirm("Are you sure you want to delete address!");
}
function dataTableInit(id, columns, ajaxUrl, orderBy) {
    orderBy = orderBy ? orderBy : []; /*[[ 3, "desc" ]]*/

    return $(id).dataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url : ajaxUrl,
            headers: { 'Authorization': 'Bearer ' +  sessionStorage.getItem("token") }
        },
        order: orderBy,
        columns: columns,
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var columnClass = column.footer().className;
                if(columnClass === 'non_searchable') {
                    $(column.footer()).empty()
                    return false
                }
                var input = document.createElement("input");
                input.placeholder = "Search "+$(column.footer()).eq(0).text();
                $(input).addClass('form-control input-sm');
                $(input).css('width', '100%');
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            });
        }
    });
}
$(function() {
    var dataColumns = [
        {data: 'title', name: 'title'},
        {data: 'name', name: 'name'},
        {data: 'number', name: 'number'},
        {data: 'address', name: 'address'},
        {data: 'default', name: 'default'},
        {data: 'type', name: 'type'},
        {data: 'actions', name: 'actions', orderable: false, searchable: false}
    ];
    dataTableInit('#example', dataColumns, ajaxUrl);
});


