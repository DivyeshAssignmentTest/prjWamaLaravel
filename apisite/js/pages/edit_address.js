$("#btn_save").click(function(){
	removeErrors($('#frm_address'));
	var response = sendAjax({
        url : apiUrl + "address/" + sessionStorage.getItem("address_id"),
        method : 'post',
        headers : {
        	'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
        },
        async : false,
        data : $('#frm_address').serialize()
    },true);

    response.done(function(dataStr) {
        window.location.href = "address.html";
    }).fail(function(jqXHR, textStatus, error) {
        var data = jqXHR.responseJSON;
        setErrors(data.errors);
    });
});

function getAddress(){
    var response = sendAjax({
        url : apiUrl + "address/" + sessionStorage.getItem("address_id"),
        method : 'get',
        headers : {
            'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
        },
        async : false
    },true);

    response.done(function(dataStr) {
        setValues(dataStr.data);
    }).fail(function(jqXHR, textStatus, error) {
        var data = jqXHR.responseJSON;
        setErrors(data.errors);
    });
}

getAddress();

function setValues(data) {
    console.log(data);
    $(`[name=title]`).val(data.title);
    $(`[name=name]`).val(data.name);
    $(`[name=number]`).val(data.number); 

    $(`[name=address_line_1]`).val(data.address_line_1);
    $(`[name=address_line_2]`).val(data.address_line_2);
    $(`[name=address_line_3]`).val(data.address_line_3);


    $(`[name=city_slug]`).val(data.city_slug);
    $(`[name=state_slug]`).val(data.state_slug);
    $(`[name=country_slug]`).val(data.country_slug);
    $(`[name=pincode]`).val(data.pincode);

    if(data.is_default == 1){
        $(`[name=is_default]`).prop('checked',true);
    }
    
    if(data.type == 'from'){
        $('#rad_from').prop('checked',true);
    }else{
        $('#rad_to').prop('checked',true);
    }
}