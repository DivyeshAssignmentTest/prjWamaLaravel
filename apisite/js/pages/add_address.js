$("#btn_save").click(function(){
	removeErrors($('#frm_address'));
	var response = sendAjax({
        url : apiUrl + "address",
        method : 'post',
        headers : {
        	'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
        },
        async : false,
        data : $('#frm_address').serialize()
    },true);

    response.done(function(dataStr) {
    	alert("Success");
        window.location.href = "address.html";
    }).fail(function(jqXHR, textStatus, error) {
        console.log(jqXHR,textStatus,error);
        var data = jqXHR.responseJSON;
        setErrors(data.errors);
    });
});

