function getProfile(){
    var response = sendAjax({
        url : apiUrl + "profile",
        method : 'get',
        headers : {
            'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
        },
        async : false
    },true);

    response.done(function(dataStr) {
        sessionStorage.setItem("username", dataStr.data.name);
        sessionStorage.setItem("email", dataStr.data.email);
        setHeader();
    }).fail(function(jqXHR, textStatus, error) {
        var data = jqXHR.responseJSON;
        console.log(data.errors);
    });
}

getProfile();
