function validateUser(){
	var $token = sessionStorage.getItem("token");
	if($token == "" || $token == undefined){
		window.location.href = "index.html";
	}else{
		console.log($token);
	}
}
validateUser();

function sendAjax(options, isThirdParty) {
    return $.ajax(options);
}

function removeErrors(form) {
    form.find('span').html('');
}

function setErrors(errors) {
    for (var key in errors){
        var input = $(`[name=${key}]`).closest('.form-group');
        var elm = input.find('span');
        var ul = '<ul class="location-style">';
        errors[key].forEach(function(val) {
            ul += `<li style="color:red;">${val}</li>`;
        });
        ul += '</ul>';
        elm.html(ul);
        input.addClass("has-error has-danger");
    }
}

function setHeader(){
	$('#bd-versions').html(sessionStorage.getItem("username"));
}

setHeader();

function logout(){
    var response = sendAjax({
        url : apiUrl + "logout",
        method : 'post',
        headers : {
            'Authorization': 'Bearer ' +  sessionStorage.getItem("token")
        },
        async : false
    },true);

    response.done(function(dataStr) {
        sessionStorage.clear();
        window.location.href = "index.html";
    }).fail(function(jqXHR, textStatus, error) {
        var data = jqXHR.responseJSON;
        console.log(data.errors);
    });
}