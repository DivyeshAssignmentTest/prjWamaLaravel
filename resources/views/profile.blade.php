@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <div style="display: inline;"><b>Manage Pofile</b></div>
                    <div style="float: right">
                        <button class="btn btn-primary" type="submit" form="base-form">Save</button>
                    </div>
                </div>

                <div class="card-body">
                    {!! Form::open(['route' => 'profile.update','id' => 'base-form']) !!}
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::text('name', $user->name, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Name *']) !!}

                            @if ($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            {!! Form::text('email', $user->email, ["class" => 'form-control input-sm', 'required' => true, 'readonly' => true, 'placeholder' => 'Email']) !!}

                            @if ($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>  
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection