<script type="text/javascript">
	function dataTableInit(id, columns, ajaxUrl, orderBy) {
		orderBy = orderBy ? orderBy : []; /*[[ 3, "desc" ]]*/

		return $(id).dataTable({
			processing: true,
			serverSide: true,
			responsive: true,
			ajax: ajaxUrl,
			order: orderBy,
			columns: columns,
			initComplete: function () {
				this.api().columns().every(function () {
					var column = this;
					var columnClass = column.footer().className;
					if(columnClass === 'non_searchable') {
						$(column.footer()).empty()
						return false
					}
					var input = document.createElement("input");
					input.placeholder = "Search "+$(column.footer()).eq(0).text();
					$(input).addClass('form-control input-sm');
					$(input).css('width', '100%');
					$(input).appendTo($(column.footer()).empty())
					.on('change', function () {
						column.search($(this).val(), false, false, true).draw();
					});
				});
			}
		});
	}
</script>