@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div style="display: inline;"><b>Manage Address Book</b></div>
                    <div style="float: right">
                        <a class="btn btn-xs btn-primary" href="">Add</a>
                    </div>
                </div>

                <div class="card-body">
                    <table id="table" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Number</th>
                                <th>Address</th>
                                <th>Default</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Name</th>
                                <th>Number</th>
                                <th>Address</th>
                                <th>Default</th>
                                <th>Type</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/datatable/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/font-awesome/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugin/datatable/jquery.dataTables.min.css') }}">
@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('plugin/datatable/jquery.dataTables.min.js') }}"></script>
    <script>
        var ajaxUrl = '{!! route("address.ajax") !!}';
        $(function() {
            var dataColumns = [
                {data: 'title', name: 'title'},
                {data: 'name', name: 'name'},
                {data: 'number', name: 'number'},
                {data: 'address', name: 'address'},
                {data: 'default', name: 'default'},
                {data: 'type', name: 'type'},
                {data: 'actions', name: 'actions', orderable: false, searchable: false}
            ];
            dataTableInit('#table', dataColumns, ajaxUrl);
        });
        function confirmDelete(){
            return confirm("Are you sure you want to delete address!");
        }
    </script>
@endsection