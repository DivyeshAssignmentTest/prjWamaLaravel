<div class="row">
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
			{!! Form::text('title', $address->title, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Title *']) !!}

			@if ($errors->has('title'))
				<span class="help-block">{{ $errors->first('title') }}</span>
			@endif
		</div>	
		<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
			{!! Form::text('name', $address->name, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Name *']) !!}

			@if ($errors->has('name'))
				<span class="help-block">{{ $errors->first('name') }}</span>
			@endif
		</div>
		<div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
			{!! Form::text('number', $address->number, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Number *']) !!}

			@if ($errors->has('number'))
				<span class="help-block">{{ $errors->first('number') }}</span>
			@endif
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
					@if($address->type == "from")
						{!! Form::radio("type", 'from',true) !!} From
					@else
						{!! Form::radio("type", 'from',false) !!} From
					@endif

					@if ($errors->has('type'))
						<span class="help-block">{{ $errors->first('type') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
					@if($address->type == "to")
						{!! Form::radio("type", 'to',true) !!} To
					@else
						{!! Form::radio("type", 'to',false) !!} To
					@endif

					@if ($errors->has('type'))
						<span class="help-block">{{ $errors->first('type') }}</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group {{ $errors->has('is_default') ? 'has-error' : '' }}">
			@if($address->is_default == 1)
				{!! Form::checkbox("is_default", '1',true) !!} Default
			@else
				{!! Form::checkbox("is_default", '1',false) !!} Default
			@endif
			@if ($errors->has('is_default'))
				<span class="help-block">{{ $errors->first('is_default') }}</span>
			@endif
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group {{ $errors->has('address_line_1') ? 'has-error' : '' }}">
			{!! Form::text('address_line_1', $address->address_line_1, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Address Line 1 *']) !!}

			@if ($errors->has('address_line_1'))
				<span class="help-block">{{ $errors->first('address_line_1') }}</span>
			@endif
		</div>
		<div class="form-group {{ $errors->has('address_line_2') ? 'has-error' : '' }}">
			{!! Form::text('address_line_2', $address->address_line_2, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Address Line 2 *']) !!}

			@if ($errors->has('address_line_2'))
				<span class="help-block">{{ $errors->first('address_line_2') }}</span>
			@endif
		</div>
		<div class="form-group {{ $errors->has('address_line_3') ? 'has-error' : '' }}">
			{!! Form::text('address_line_3', $address->address_line_3, ["class" => 'form-control input-sm', 'placeholder' => 'Address Line 3']) !!}

			@if ($errors->has('address_line_3'))
				<span class="help-block">{{ $errors->first('address_line_3') }}</span>
			@endif
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group {{ $errors->has('country_slug') ? 'has-error' : '' }}">
					{!! Form::select("country_slug", $country, $address->country_slug, ['id' => 'select_country' ,'class' => 'form-control input-sm','placeholder' => 'Select Country', 'required' => true]) !!}

					@if ($errors->has('country_slug'))
						<span class="help-block">{{ $errors->first('country_slug') }}</span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('city_slug') ? 'has-error' : '' }}">
					{!! Form::select("city_slug", $city, $address->city_slug, ['id' => 'select_city' ,'class' => 'form-control input-sm','placeholder' => 'Select City', 'required' => true]) !!}

					@if ($errors->has('city_slug'))
						<span class="help-block">{{ $errors->first('city_slug') }}</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group {{ $errors->has('state_slug') ? 'has-error' : '' }}">
					{!! Form::select("state_slug", $state, $address->state_slug, ['id' => 'select_state' ,'class' => 'form-control input-sm','placeholder' => 'Select State', 'required' => true]) !!}

					@if ($errors->has('state_slug'))
						<span class="help-block">{{ $errors->first('state_slug') }}</span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('pincode') ? 'has-error' : '' }}">
					{!! Form::text('pincode', $address->pincode, ["class" => 'form-control input-sm', 'required' => true, 'placeholder' => 'Pincode']) !!}

					@if ($errors->has('pincode'))
						<span class="help-block">{{ $errors->first('pincode') }}</span>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

