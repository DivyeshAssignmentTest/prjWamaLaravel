@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div style="display: inline;"><b>Manage Address Book</b></div>
                    <div style="float: right">
                        <button class="btn btn-xs btn-primary" type="submit" form="base-form">Save</button>
                    </div>
                </div>

                <div class="card-body">
                    {!! Form::open(['route' => ['address.update',$address],'method' => 'PUT','id' => 'base-form']) !!}
                        @include('addressbook.partials.form')
                    {!! Form::close() !!} 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-css')
    
@endsection

@section('page-js')
    <script type="text/javascript">

    </script>
@endsection